## Deployments

Merging into the followiing branches in gitlab will kick-off deployments:

- develop
- master

Deployments consist of a migrations step, a build step and a deployment step. There is a .ci folder in the project root that is responsible for creating a .env file, it reads in the CI Variables and uses them to create a .env file.

- <scope:branch> : sepcifc to a branch
- <scope:branch> : global var used by all branches

### Migrations

Requires the followiing variables to be set in the CI/CD area of Settings in Gitlab.

- CONTENTFUL_ACCESS_TOKEN <get-from-contentful> <scope:branch>
- CONTENTFUL_ENVIRONMENT <get-from-contentful> <scope:branch>
- CONTENTFUL_MANAGEMENT_TOKEN <get-from-contentful> <scope:default>
- CONTENTFUL_MIGRATIONS_ID <get-from-contentful> <scope:branch>
- CONTENTFUL_SPACE_ID <get-from-contentful> <scope:default>

CONTENTFUL_MIGRATIONS_ID is the Entity ID of the migrations object. It is found in Content and is found at the end of the Migrations content path. If setting up an environment for the first time, make sure this content type and entity exists otherwise the pipe will fail.

There is a webhook configured inside Contentful so if changes are published or unpublished in either the 'develop' or 'master' branches in Contenful, a build will be kicked off from the corrosponding branch in Gitlab. The Entry ID of the migration file is used to in a rule within the webhook to prevent the migration script (which publishes migration updates) from triggering another build from within a pipeline.

### Build

Builds the app and stores the /public folder as an asset to be used in the deployment step.

### Deployment

Installs the AWS CLI and deploys the static stite (from the /public folder) into S3. It requires the following variibles:

- AWS_ACCESS_KEY_ID <get-from-aws> <scope:default>
- AWS_SECRET_ACCESS_KEY <get-from-aws> <scope:default>
- BUCKET_NAME <get-from-aws>

### Other CI/CD variables

- ENV : var used by the application to determine which environment it is running in e.g. "prod-dev"
- SITE_ADDRESS : url of the site including protocol e.g. "https://wrest.dev"
