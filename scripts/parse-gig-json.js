const fs = require('fs')
const {Client} = require("@googlemaps/google-maps-services-js");
const gigData = require("../migration-data/gig.json")
const envConf = { path: ".env.production" };
require("dotenv").config(envConf);
const client = new Client({});

(async () => {
  const posts = gigData.rss.channel.item
    .filter(gig => gig.status.__cdata ==="publish")
    .map(gig => {
      const postMetaData = {}
      gig.postmeta.forEach( (postmeta) => {
        const dataPrefixKey = "__cdata"
        const fields = [
          {oldKey: "date", newKey: "date"},
          {oldKey: "venue", newKey: "venue"},
          {oldKey: "ticket_link", newKey: "ticketLink"},
          {oldKey: "location", newKey: "location"},
          {oldKey: "sold_out", newKey: "soldOut"}
        ]


        fields.forEach( field => {
          if(postmeta.meta_key[dataPrefixKey] === field.oldKey){
            if(postmeta.meta_key[dataPrefixKey] === "sold_out"){
              postMetaData[field.newKey] = (postmeta.meta_value[dataPrefixKey] === '0') ? false : true
            } else {
              postMetaData[field.newKey] = postmeta.meta_value[dataPrefixKey]
            }

          }
        })
      })
      const venueLocation = `${postMetaData.venue} ${postMetaData.location}`

      return {
        title: gig.title.__cdata,
        date: postMetaData.date,
        venueLocation: venueLocation,
        ticketLink: postMetaData.ticketLink,
        soldOut: postMetaData.soldOut
      }
    })


  for(const [index, post] of posts.entries()){
    try{
      const mapQuery = await client.findPlaceFromText({
        params: {
          input: post.venueLocation,
          inputtype: "textquery",
          fields: "formatted_address,name,geometry",
          key: process.env.GOOGLE_MAPS_API_KEY,
        },
        timeout: 1000, // milliseconds
      })

      posts[index].locationData =  mapQuery.data.candidates
    } catch(err){
      console.error(err)
    }
    // console.log("mapQuery", mapQuery.data)
    // console.log("mapQuery.g", mapQuery.data.candidates[0].geometry)
  }

  fs.writeFileSync(`${__dirname}/../migration-data/gigs-migrated.json`, JSON.stringify(posts))
})()