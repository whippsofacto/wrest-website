#!/usr/bin/env node
/* eslint-disable no-console */
/* eslint-disable global-require */

// refactored from this example:
// https://github.com/contentful-labs/continous-delivery-environments-example/blob/master/scripts/migrate.js
const path = require("path");

(async () => {
  try {
    const [, , local] = process.argv;
    let envConf = {};
    if (local) {
      envConf = { path: ".env.production" };
    }

    require("dotenv").config(envConf);
    const utils = require("./utils");
    const { createClient } = require("contentful-management");
    const {
      default: runMigration,
    } = require("contentful-migration/built/bin/cli");

    const options = {
      CONTENTFUL_MANAGEMENT_TOKEN: process.env.CONTENTFUL_MANAGEMENT_TOKEN,
      CONTENTFUL_SPACE_ID: process.env.CONTENTFUL_SPACE_ID,
      CONTENTFUL_ACCESS_TOKEN: process.env.CONTENTFUL_ACCESS_TOKEN,
      CONTENTFUL_ENVIRONMENT: process.env.CONTENTFUL_ENVIRONMENT,
    };

    const client = createClient({
      accessToken: options.CONTENTFUL_MANAGEMENT_TOKEN,
    });

    const migrationConnectionOptions = {
      spaceId: options.CONTENTFUL_SPACE_ID,
      environmentId: options.CONTENTFUL_ENVIRONMENT,
      accessToken: options.CONTENTFUL_MANAGEMENT_TOKEN,
      yes: true,
    };




    console.log("Getting space.");
    const space = await client.getSpace(options.CONTENTFUL_SPACE_ID);
    console.log(`Getting environmment: ${options.CONTENTFUL_ENVIRONMENT}.`);
    const environment = await space.getEnvironment(
      options.CONTENTFUL_ENVIRONMENT
    );

    const defaultLocale = (await environment.getLocales()).items.find(
      (locale) => locale.default
    ).code;

    console.log(`Default locale for environment: ${defaultLocale}`);

    // ---------------------------------------------------------------------------
    console.log("Read all the available migrations from the file system");
    const availableMigrations = await utils.availableMigrations();
    console.log(availableMigrations);

    // ---------------------------------------------------------------------------
    console.log("Figure out latest ran migration of the contentful space");
    const entry = await environment.getEntry(
      process.env.CONTENTFUL_MIGRATIONS_ID
    );
    const { migrationFilesRun } = entry.fields.migrations[defaultLocale];

    console.log("Previously run migrations", migrationFilesRun);

    // // ---------------------------------------------------------------------------
    console.log("Evaluate which migrations to run");
    const migrationsToRun = [];
    availableMigrations.forEach((migration) => {
      if (!migrationFilesRun.includes(migration)) {
        migrationsToRun.push(migration);
      }
    });

    console.log("Migrations still to run: ");
    console.log(migrationsToRun);

    // // ---------------------------------------------------------------------------

    if (migrationsToRun.length) {
      console.log("Run migrations and update version entry");
      const updatedMigrationsList = [...migrationFilesRun];
      for (const migration of migrationsToRun) {
        const filePath = path.join(__dirname, "..", "migrations", migration);

        console.log(`Running ${filePath}`);
        await runMigration(
          Object.assign(migrationConnectionOptions, {
            filePath,
          })
        );


        const { seed } = require(filePath)
        if (seed) {
          console.log(`${filePath} contains seed data...`)
          await seed(environment, defaultLocale)
        }

        console.log("Updating migrations list.");
        updatedMigrationsList.push(migration);
        await entry.patch([
          {
            op: "replace",
            path: `/fields/migrations/${defaultLocale}`,
            value: { migrationFilesRun: updatedMigrationsList },
          },
        ]);

        const patchedEntry = await environment.getEntry(
          process.env.CONTENTFUL_MIGRATIONS_ID
        );

        await patchedEntry.publish();
      }



      console.log("Done.");
    } else {
      console.log("No new migration files.");
    }
  } catch (err) {
    console.log("Migrations Error");
    console.log(err);
    process.exit(1);
  }
})();
