#!/usr/bin/env node

const path = require("path");
const { promisify } = require("util");
const { readdir } = require("fs");
const readdirAsync = promisify(readdir);
const MIGRATIONS_DIR = path.join(".", "migrations");
// utility fns
const getVersionOfFile = (file) => file.replace(".migration.js", "")
const getFileOfVersion = (version) => version + "migration.js";

const availableMigrations = async () =>
 (await readdirAsync(MIGRATIONS_DIR))
    .map((file) => file).sort((a,b) => b - a);

module.exports = {
    availableMigrations,
    getVersionOfFile,
    getFileOfVersion
}