
const fs = require("fs").promises
const utils = require("./utils")
const templates = require("./templates");


(async () => {
  const  [,,fileName] = process.argv

  if(!fileName){
    console.log("You must provide a file name.")
    return
  }

  const availableMigrations = await utils.availableMigrations()

  if(availableMigrations.includes(utils.getVersionOfFile(fileName))){
    console.log(`Migration with this name already exists, please use another: "${fileName}"`)
    return
  }
  const now = new Date()
  const year = now.getFullYear()
  const month =  ("0" + (now.getMonth() + 1)).slice(-2)
  const date = ("0" + now.getDate()).slice(-2)
  const hours = now.getHours()
  const minutes = now.getMinutes()
  const seconds = now.getSeconds()
  const dateString = `${year}${month}${date}${hours}${minutes}${seconds}`

  const fileNameWithType = `${dateString}_${fileName}.migration.js`
  await fs.writeFile(`./migrations/${fileNameWithType}`, templates.BASIC_MIGRATION)
  console.log("Success: Migration File Created")

})()
