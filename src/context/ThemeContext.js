import * as React from 'react';
import  ThemeProvider  from '@mui/material/styles/ThemeProvider';
import CssBaseline from "@mui/material/CssBaseline";
import theme from "../theme/index"



const Theme = ({children}) => {
  console.log("theme", theme)
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
}

export default Theme