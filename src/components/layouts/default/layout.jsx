import React from "react";
import Nav from "../../core/navigation/Navigation"

class Template extends React.Component {
  render() {
    const { children } = this.props;

    return (
      <>
        <Nav />
        {children}
      </>
    );
  }
}

export default Template;
