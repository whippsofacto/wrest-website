import React from 'react';
import Box from '@mui/material/Box'
import styles from "./Lyrics.style"
import Typed from "../core/typed/Typed"
import { keyframes } from '@emotion/react';
import { v4 as uuidv4 } from 'uuid';

function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}
function shuffle(array) {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex != 0) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

const leftPosition = () => getRandomArbitrary(0,90)
const topPosition = () => getRandomArbitrary(0,90)
const FONT_SIZES = ['0.75rem', '1rem', "1.5rem", "2rem"]
const ANIMATION_SPEED = ["2s", '3s', '6s', "9s", "12s", "16s"]
const generateRandomAnimationSpeed = () => ANIMATION_SPEED[getRandomArbitrary(0, ANIMATION_SPEED.length)]
const generateRandomFontSize = () => FONT_SIZES[getRandomArbitrary(0, FONT_SIZES.length)]
const randomTypeSpeed = () => getRandomArbitrary(75,200)
const flicker = keyframes`
     0%  { opacity:0;},
     50% { opacity: .6;},
     100% { opacity:0;}`
const move = keyframes`
     0%  {margin-top: 0px},
     50% {  margin-top: 10px},
     100% { opacity:0; margin-top: 30px}`


const LYRICS = [
  "Open your eyelids, asleep on the job",
  "picture postcards from a far.",
  "I remember what you said to me.",
  "we can both breathe",
  "It's the end of all the days and the days came quickly",
  "to you my kingdom",
  "stones in your glasshouse",
  "eyes clear enough to see through",
  "what are you afraid of?",
  "are you feeling alright",
  "I am wanderng",
  'a miracle, any time now',
  'we could\'ve been honest',
  "I would love you forever",
  "Can we take this outside?",
  'Set your soul on fire',
  "It's all lost on me",
  "Hopeless tragedy",
  "maybe it wasn't meant to be",
  "Will I ever see you again?",
  "Everything I ever asked for is right in front of me",
  "You chill me to the bone",
  "you cut me from it",
  "slowly but surely",
  'we can both breathe for our sins'
]


const lyrics = [...shuffle(LYRICS), ...shuffle(LYRICS), ...shuffle(LYRICS),  ...shuffle(LYRICS), ...shuffle(LYRICS)]

const shapeLyrics = () => {
  const arrayofQuadrants = [
    [],[],[],[]
  ]
  lyrics.forEach((lyric, index) => {
    let idx = index % 4
    arrayofQuadrants[idx].push({string: lyric})
  })

  return arrayofQuadrants
}

const quads = [{
  id: 1,
  styles: {
    left:0,
    top: 0,
  },
  lyricStyles: {
  },
  lyrics: shapeLyrics()[0]
},
{
  id: 2,
  styles: {
    left:0,
    bottom: 0,
  },
  lyricStyles: {
  },
  lyrics: shapeLyrics()[1]
},
{
  id: 3,
  styles: {
    right:0,
    bottom: 0,
  },
  lyricStyles: {
  },
  lyrics: shapeLyrics()[2]
},
{
  id: 4,
  styles: {
    right:0,
    top: 0,
  },
  lyricStyles: {
  },
  lyrics: shapeLyrics()[3]
},
]


const Quad = ({lyrics, lyricStyles}) => lyrics.map(lyric => {
  let rain = ''
  const random = getRandomArbitrary(0, 100)
  console.log()
  if(random % 5 === 0){
    rain = `${move} ${generateRandomAnimationSpeed()} infinite ease-in`
  }
  return(
    <Box sx={{...styles.lyrics, top: `${topPosition()}%` , left: `${leftPosition()}%`, ...lyricStyles, animation: rain }} key={uuidv4()}>
      <Typed strings={[lyric.string]}  speed={randomTypeSpeed()} sx={{...styles.typed, fontSize: generateRandomFontSize(), animation: `${flicker} ${generateRandomAnimationSpeed()} infinite ease-in-out`}}/>
    </Box>

  )
})




const Lyrics = () => {
  return (
    <Box sx={styles.container}>
      {
        quads.map( quad => {
          return (
            <Box key={quad.id} sx={{...styles.quad, ...quad.styles}}>
              <Quad lyrics={quad.lyrics} lyricStyles={quad.lyricStyles}/>
            </Box>
          )
        })
      }
    </Box>
  );
};



export default Lyrics