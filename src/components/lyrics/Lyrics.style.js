const styles = {
  container: {
    height: "100%",
    width: "100%",
    position: "absolute",
    left: 0,
    top: 0,
  },
  lyrics: {
    position: "absolute"
  },
  quad: {
    position:"absolute",
    width: "50%",
    height:  "50%",
    zIndex: 0
  },
  typed: {
    position: "relative"
  },
}




export default styles