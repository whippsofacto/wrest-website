

const style = {
  header: {
    backgroundColor: "white"
  },
  linkContainer: {
    justifyContent: "flex-end",
    flexGrow: 1,
    display: { xs: 'none', md: 'flex' }
  },
  link: {
    fontWeight: 'bold',
    fontSize: "1.3rem",
    marginLeft: "1rem",
  }
}

export default style