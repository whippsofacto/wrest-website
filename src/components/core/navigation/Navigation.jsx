import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Drawer from '@mui/material/Drawer';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import MenuItem from '@mui/material/MenuItem';
import styles from "./Navigation.style"
import Socials from "../social/Socials"

const pages = ['Gigs', "Media", "Shop", "Music"];

const ResponsiveAppBar = () => {
  const [openNav, setOpenNav] = React.useState(false);



  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setOpenNav(open);
  };



  return (
    <>
      <AppBar position="static" color="default">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Typography
              variant="brand"
              noWrap
              component="div"
              sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
            >
            wrest
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={toggleDrawer(true)}
              >
                <MenuIcon />
              </IconButton>
            </Box>
            <Typography
              variant="brand"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
            >
            wrest
            </Typography>
            <Box sx={ styles.linkContainer}>
              <Box sx={{marginRight: "1rem",  display: "flex", alignItems: "center"}}>
                {pages.map((page) => (
                  <Box key={page} sx={styles.link}>
                    {page}
                  </Box>
                ))}
              </Box>
              <Socials sx={{marginLeft: "1rem"}} />
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Drawer
        open={openNav}
        onClose={toggleDrawer(false)}
      >
        {pages.map((page) => (
          <MenuItem key={page}>
            <Typography textAlign="center">{page}</Typography>
          </MenuItem>
        ))}
      </Drawer>
    </>
  );
};
export default ResponsiveAppBar;
