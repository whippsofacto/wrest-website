import React from 'react';
import PropTypes from 'prop-types';
import Container from "@mui/material/Container"
import Box from "@mui/material/Box"
import DynamicImage  from "../../media/DynamicImage/Img"
import Lyrics from "../../../lyrics/Lyrics"
import Link from "../../links/LinkAsButton/LinkAsButton"
import styles from "./Hero.style"
import { useTheme } from '@mui/styles'


const Hero = ({imageData, ctaText, ctaLink, backgroundColour, backgroundImageData}) => {
  const theme = useTheme();
  return (
    <Box sx={{ background: backgroundColour, position: "relative", marginBottom: theme.spacing(10) }}>
      <Container sx={styles.container}>
        <Box sx={styles.image}>
          <DynamicImage imageData={imageData} alt={imageData.description} />
          <Link  sx={styles.button} linkText={ctaText} href={ctaLink}/>
        </Box>

      </Container>
      {backgroundImageData  && <Box sx={styles.backgroundImage}> <DynamicImage imageData={backgroundImageData} alt={backgroundImageData.description} />        </Box>}
      <Lyrics />
    </Box>
  );
};

Hero.defaultProps = {
  backgroundColour: undefined
}

Hero.propTypes = {
  imageData: PropTypes.shape({}),
  backgroundColour: PropTypes.string,
  ctaLink: PropTypes.string,
  ctaText: PropTypes.string

};
export default Hero