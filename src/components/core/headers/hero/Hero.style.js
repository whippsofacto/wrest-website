
const styles = {
  container:{
    display:"flex",
    flexDirection: "column",
    alignItems: "center",
    position: "relative",
    zIndex: 1,
  },
  image:{},
  backgroundImage: {
    position: "absolute",
    width:" 100%",
    height:" 100%",
    opacity: "0.9",
    top: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    position: "absolute",
    right: "200px",
    bottom: "27%",
    background: "rgba(255,255,255,0.8)",
    fontSize: "1.2rem",
    fontWeight: "bold",
    "&:hover": {
      tansition: "0.3s ease-in",
      background: "rgba(255,255,255,1)",
    }
  }
}

export default styles