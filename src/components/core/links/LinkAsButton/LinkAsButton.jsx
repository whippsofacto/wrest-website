import React from "react"
import Link from '@mui/material/Link';

const LinkButton = ({href, linkText, sx}) => {
  return (
    <Link
      sx={sx}
      href={href}
      variant="cta"
    >
      {linkText}
    </Link>
  );
}

export default  LinkButton