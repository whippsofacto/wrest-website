import React from 'react';
import PropTypes from 'prop-types';
import Container  from '@mui/material/Container';
import Box from "@mui/material/Box"

const GigsList = ({gigs, limit}) => {

  if(limit){
    gigs = gigs.filter((gig, index) => limit < index)
  }

  return (
    <Container>
      {
        gigs.map((gig) => {
          console.log("gig", gig)
          return (
            <Box key={gig.id}>
              {gig.title}
            </Box>
          )
        })
      }
    </Container>
  );
};


GigsList.defaultProps = {
  limit: undefined
}

GigsList.propTypes = {
  limit: PropTypes.number,
  gigs:PropTypes.arrayOf(PropTypes.shape({}))
};
export default GigsList