
import React from "react"
import PropTypes from 'prop-types'
import { GatsbyImage, getImage } from "gatsby-plugin-image"


const Img = ({imageData, alt}) => {
  const image = getImage(imageData)
  return (
    <GatsbyImage image={image} alt={alt} />
  )
}

Img.propTypes = {
  imageData: PropTypes.shape({}),
  alt: PropTypes.string
}

export default Img