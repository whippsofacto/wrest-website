import React, {useRef} from 'react';
import Typography from "@mui/material/Typography"
import styles from './Typed.style'
import Typed from 'typed.js';

const Type = ({strings, sx, speed}) => {
  const el = useRef(null)
  // Create reference to store the Typed instance itself
  const typed = React.useRef(null);
  React.useEffect(() => {
    const options = {
      strings: strings,
      showCursor: false,
      typeSpeed: speed,
      loop: true
    };

    // elRef refers to the <span> rendered below
    typed.current = new Typed(el.current, options);

    return () => {
      // Make sure to destroy Typed instance during cleanup
      // to prevent memory leaks
      typed.current.destroy();
    }
  }, [])


  return (
    <Typography variant="handwriting" component="div" ref={el} sx={{...styles.typed, ...sx}} />
  );
};



export default Type