import React from "react"
import Link from "@mui/material/Link"
import Box from '@mui/material/Box'
import fb from "../../../assets/socials/fb.svg"
import yt from "../../../assets/socials/yt.svg"
import spotify from "../../../assets/socials/spotify.svg"
import twitter from "../../../assets/socials/twitter.svg"
import ig from "../../../assets/socials/ig.png"

const socials = [
  {href: "https://facebook.com/wearewrest", src: fb },
  {href: "https://instagram.com/wearewrest", src: ig },
  {href: "https://twitter.com/wearewrest", src: twitter },
  {href: "https://spotify.com/wearewrest", src: spotify },
  {href: "https://youtube.com/wearewrest", src: yt }
]

const Socials = ({sx}) => {
  return (
    <Box sx={{display: "flex", alignItems: "center", ...sx}}>
      {
        socials.map(social => {
          return(
            <Link href={social.href} key={social.href}>
              <img src={social.src}  style={{height: "20px", width: "auto", marginLeft: "10px"}}/>
            </Link>
          )
        })
      }
    </Box>
  )
}

export default Socials