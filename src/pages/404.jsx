import React from "react";
import Layout from "../components/layouts/default/layout";

function FourOhFour() {
  return (
    <Layout>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <h1>404</h1>
      </div>
    </Layout>
  );
}

export default FourOhFour;
