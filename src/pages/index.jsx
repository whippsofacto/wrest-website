import React from "react";
import { graphql } from "gatsby";
import get from "lodash/get";
import { Helmet } from "react-helmet";
import Layout from "../components/layouts/default/layout";
import Container from "@mui/material/Container"
import Hero from "../components/core/headers/hero/Hero"
import GigsList from "../components/core/gigs-list/GigsList"

class RootIndex extends React.Component {
  render() {
    const pageData = get(this, "props.data.allContentfulHomeTemplate.nodes")[0];
    const gigData = get(this, "props.data.allContentfulGig.nodes")
    const headerData = pageData.header


    return (
      <Layout location={this.props.location}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>wrest - End All the Days - Coming Soon</title>
          <link rel="canonical" href="/" />
          <meta property="description" content="Edinburgh based indie band. New album End All the Days out March 4th" />
        </Helmet>
        <Hero
          backgroundImageData={headerData.backgroundImage}
          backgroundColour={headerData.backgroundColour}
          ctaLink={headerData.ctaLink}
          ctaText={headerData.ctaText}
          imageData={headerData.image}
          heading={headerData.heading}
          tagline={headerData.tagline}
        />
        <Container>
          {
            pageData.includeGigs && <GigsList gigs={gigData}/>
          }
        </Container>
      </Layout>
    );
  }
}

export default RootIndex;

export const pageQuery = graphql`
  query HomeQuery {
    allContentfulHomeTemplate {
      nodes {
        includeGigs
        header {
          createdAt
          ctaText
          ctaLink
          heading
          backgroundColour
          image {
            description
            gatsbyImageData(
               width: 1000,
               height: 1000,
               placeholder: BLURRED,
               formats: [WEBP],
               resizingBehavior: PAD
             )
          }
        }
      }
    }
    allContentfulGig(sort: {order: DESC, fields: date}) {
        nodes {
          title
          ticketLink
          venueLocation {
            lat
            lon
          }
          date
          id
        }
      }
  }
`;
