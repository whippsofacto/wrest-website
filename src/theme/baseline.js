import typography from "./typography"
const baseline = {
  MuiCssBaseline: {
    styleOverrides: {
      body: {
        margin: 0
      },
      "@font-face": [
        typography.Caveat,
      ],
    }
  }
}


export default baseline