const colors = {
  primary: {
    main: '#0a5182',
  },
  secondary: {
    main: '#008faf',
  },
  black: {
    main: '#010101'
  }
}

export default colors
