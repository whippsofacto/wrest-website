import Caveat from '../fonts/Caveat/static/Caveat-Regular.ttf'
import EB_Garamond from '../fonts/EB_Garamond/static/EBGaramond-Regular.ttf'


const variants = {
  handwriting:{
    fontFamily: "Caveat"
  },
  brand: {
    fontFamily: "EB_Garamond",
    fontSize: '2.2rem'
  }
}

const styleOverrides = {
  Caveat: {
    fontFamily: "Caveat",
    fontStyle: "normal",
    fontDisplay: "swap",
    fontWeight: 400,
    src:
           `local('Caveat'), local('Caveat-Regular'), url(${Caveat}) format('truetype')`
  },
  Brand: {
    fontFamily: "EB_Garamond",
    fontStyle: "normal",
    fontDisplay: "swap",
    fontWeight: 400,
    src:
           `local('EB_Garamond'), local('EB_Garamond-Regular'), url(${EB_Garamond}) format('truetype')`
  }
}

const typography = {
  ...styleOverrides,
  ...variants
}

export default typography
