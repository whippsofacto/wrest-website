import { createTheme } from '@mui/material/styles';
import baseline from './baseline'
import colors from "./colors"
import links from './links'
import typography from './typography';
const   fontFamily = [
  'Overpass',
  '"Helvetica Neue"',
  'Arial',
  'sans-serif',
  '"Apple Color Emoji"',
  '"Segoe UI Emoji"',
  '"Segoe UI Symbol"',
].join(',')
// A custom theme for this app
const theme = createTheme({

  components: {
    ...baseline,
    ...links
  },
  palette: {
    ...colors
  },
  typography:{
    fontFamily: fontFamily,
    brand: typography.brand,
    handwriting: typography.handwriting
  }
});

export default theme;