const links = {
  MuiLink: {
    styleOverrides: {
      root: ({ ownerState, theme }) =>
      {
        return {
          ...(ownerState.variant === 'cta' && {
            ...theme.typography.h4,
            border: "1px solid black",
            borderRadius: theme.shape.borderRadius,
            padding: "20px",
            textDecoration: "none"
          }),
        }
      },
    },
  }
}

export default links