import React from "react"
import ThemeProvider  from "./src/context/ThemeContext"
import "@fontsource/overpass"

export const wrapRootElement = ({ element }) => (
  <ThemeProvider>{element}</ThemeProvider>
)