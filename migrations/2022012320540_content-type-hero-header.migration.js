
module.exports = async function(migration) {
  const contentType = migration
    .createContentType("heroHeader")
    .name("Hero Header")
    .description("Component - type of page header.")
    .displayField("heading");

  contentType.createField("image")
    .name("Image")
    .type("Link")
    .validations([
      {
        "linkMimetypeGroup": [
          "image"
        ]
      }
    ])
    .linkType("Asset")
  contentType.createField("heading").name("Heading").type("Symbol");
  contentType.createField("ctaLink").name("CTA link").type("Symbol").validations([
    {
      "regexp": {
        "pattern": "^(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-/]))?$",
        "flags": null
      },
      "message": "must be a valid url"
    }
  ])
  contentType.createField("ctaText").name("CTA Text").type("Symbol").validations([
    {
      "size": {
        "min": null,
        "max": 200
      }
    }
  ]);
  contentType.createField("tagline").name("Tagline").type("Text");
  contentType.createField("backgroundColour").name("Background Colour")
    .type("Symbol")
    .validations([
      {
        "regexp": {
          "pattern": "^#(?:[0-9a-fA-F]{3}){1,2}$",
          "flags": null
        },
        "message": "Must be a valid hex colour value."
      }
    ])

};