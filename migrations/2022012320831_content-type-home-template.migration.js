
module.exports = async function(migration) {
  const contentType = migration
    .createContentType("page")
    .name("Home Template")
    .displayField("slug");

  contentType.createField("header").name("Header").type("Link").linkType("Entry")
  contentType.createField("slug").name("Slug").type("Symbol").validations( [
    {
      "unique": true
    }
  ],)
  contentType.createField("includeGigs").name("Include Gigs").type("Boolean").defaultValue({"en-GB": true});
};