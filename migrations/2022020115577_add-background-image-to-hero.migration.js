
module.exports = async function(migration) {
  const contentType = migration.editContentType("heroHeader")
  contentType.createField("backgroundImage")
    .name("Background Image")
    .type("Link")
    .validations([
      {
        "linkMimetypeGroup": [
          "image"
        ]
      }
    ])
    .linkType("Asset")
};