const gigData = require("../migration-data/saves/gigs-migrated.json")
const _ = require("lodash")

module.exports = async function(migration) {};


// Seed sample data
module.exports.seed = async (client, defaultLocale) => {


  for(const [, gig] of gigData.entries()){

    console.log(`${gig.title} - - - - - - - - - - - - - - - - - - - - - - - `)
    await client.createEntry('gigs', {
      fields: {
        title: {
          [defaultLocale]: gig.title
        },
        date: {
          [defaultLocale] : new Date(gig.date).toISOString()
        },
        venueLocation: {
          [defaultLocale]: {
            "lat": gig.locationData[0].geometry.location.lat, "lon": gig.locationData[0].geometry.location.lng
          }
        },
        ticketLink: {
          [defaultLocale] : (gig.ticketLink) ? gig.ticketLink : ""
        },
        soldOut: {
          [defaultLocale]: (_.has(gig,"soldOut") ? true : false)
        }
      },
    })
  }



  // gigsEntry.publish()

}
