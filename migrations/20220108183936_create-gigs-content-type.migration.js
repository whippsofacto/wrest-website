module.exports = async function (migration) {
  const gigs = migration
    .createContentType("gigs")
    .name("Gig")
    .description(
      "Data pertaining to shows such as venue, time, status, date or ticket"
    )
    .displayField("title");

  gigs.createField("title").name("Title").type("Symbol").required(true);
  gigs.createField("date").name("Date").type("Date");
  gigs.createField("venueLocation").name("Venue Location").type("Location");
  gigs.createField("ticketLink")
    .name("Ticket Link")
    .type("Symbol")
    .validations([
      {
        "size": {
          "min": 0,
          "max": 800
        },
        "message": "Link must not be longer than 800 chars"
      },
      {
        "regexp": {
          "pattern": "^(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-/]))?$",
          "flags": null
        },
        "message": "Must be a valid link."
      }
    ])
  gigs.createField("soldOut").name("Mark as sold out").type("Boolean");

};
